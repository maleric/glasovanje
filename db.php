<?php

class Database extends PDO {
    
    function __construct() {
        require 'config/database.php';
        parent::__construct(DB_TYPE.':host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASS);
            
    }
     public function insert($user) {
        $sth = $this->prepare("INSERT INTO users VALUES (:ip, :vote, :voted)");
        $send=$sth->execute(array(
                ':ip' => $user->ip,
                ':vote' => $user->vote,
                ':voted' => $user->voted
                ));
        
        //start session with voted
        session_start();
        $_SESSION["user"]=true;
        
    }
    public function getData() {
        $sth= $this->prepare("SELECT * FROM users");
        $sth->execute();
        $data=$sth->fetchAll(PDO::FETCH_ASSOC);
        return $data;
        
    }
    public function getUser($ip) {
        $sth= $this->prepare("SELECT * FROM users WHERE "
                . "ip = :ip");
        $sth->execute(array(':ip' => $ip ));
        $data=$sth->fetch();
        return $data;
        
    }
    public function getTeams() {
        $sth= $this->prepare("SELECT * FROM timovi");
        $sth->execute();
        $data=$sth->fetchAll(PDO::FETCH_ASSOC);
        return $data;
        
    }
}


