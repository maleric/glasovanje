<?php

require_once('user.php');
require_once('db.php');
session_start();

$user= new User();
$db=new Database();
$user_exists = $db->getUser($user->ip);
$voted=false;
if ($user_exists) {
    echo "KORISNIK JE GLASAO";
    header("Location: statistika.php");
    exit();
}

if (isset($_SESSION["user"])) {
    echo "SESIJA POSTOJI";
    $voted=true;
    header("Location: statistika.php");
    exit();
}  
?>

<!doctype html>
<html class="no-js" lang="hr">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Glasovanje</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/custom.css" />
    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>

    <div class="row">
      <div class="large-12 columns">
        <h1>Odaberite favorita:</h1>
      </div>
    </div>
    
    <div clas="row">
	<div class="small-12 column">
            <form id="forma1" name="unos" method="POST" action="form_submit.php" >
            <label>
                <input type="text" class="expand" id="oznaka" name="oznaka" value="<?php echo $user->ip ?>" readonly>
            </label>
         
	</div>
	
	<div class="small-12 column">
	<label>
	<select name="tim">
            <option value="1">FOI II (FOI dva)</option>
            <option value="2">FOI I (FOI jedan)</option>
            <option value="3">Abs Int</option>
            <option value="4">HakunaMatata</option>
            <option value="5">TeamRocket</option>
            <option value="6">IT Celije</option>
            <option value="7">Loopus</option>
            <option value="8">Little endians</option>
            <option value="9">Inversity</option>
            <option value="10">BIG Team</option>
            <option value="11">(log2)</option>
	</select>
	</label>
         
	</div>
	
	<div class="small-12 column">
	<input type="submit" name="posalji" value="Pošalji" class="button success expand">
        
        </form>
	</div>
	
	</div>
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
  </body>
</html>
