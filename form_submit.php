<?php


require_once('user.php');
require_once('db.php');
$user = new User();
$db = new Database();

$user_exists = $db->getUser($user->ip);
if ($user_exists || isset($_SESSION["user"])) {
    header("Location: statistika.php");
    exit();
}

if(isset($_POST['posalji'])){
    session_start();
    
    
    $oznaka = $_POST['oznaka'];
    $tim = $_POST['tim'];
           
    $user->setParams($tim, true);
    $db->insert($user);
    header("Location: statistika.php");
}
