<?php

require_once('db.php');
$db = new Database();

$data = $db->getData();
$teamData = $db->getTeams();
//var_dump($teamData);
$statistika = array();
foreach ($data as $k => $value) {
    //echo $value["vote"];
    if (empty($statistika[$value["vote"]])) {
        $statistika[$value["vote"]] = 1;
    } else {
        $statistika[$value["vote"]]+=1;
    }
}


$teamPodaci = "";
$brojTimova = 11;
/*for ($i = 0; $i < $brojTimova; $i++) {
    if ($i < $brojTimova-1) {
        $teamPodaci .= "" . $teamData[$i]["Naziv"] . ",";
    } else {
        $teamPodaci .= "" . $teamData[$i]["Naziv"];
    }
}*/

echo $teamPodaci;
$podaci = "";

for ($i = 1; $i < $brojTimova + 1; $i++) {
    $podatak = 0;
    if (isset($statistika[$i])) {
        $podatak = $statistika[$i];
    }
    if ($i < $brojTimova) {
        $podaci .= "" . $podatak . ",";
    } else {
        $podaci .= "" . $podatak;
    }
}
//echo $podaci;
?>
<!doctype html>
<html class="no-js" lang="hr">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Foundation | Welcome</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <link href="css/jquery.circliful.css" rel="stylesheet" type="text/css" />

    <script src="Chart.js"></script>
    <style>
        canvas{
        }
    </style>
  </head>
</head>
<body>
    <div class="row">
      <div class="large-12 columns">
        <h1>Statistika glasovanja</h1>
        <br>
        <canvas id="canvas" height="450" width="600"></canvas>
      </div>
    </div>
    
    <script>

		var barChartData = {
			labels : [<?php 
                                    for ($i = 0; $i < $brojTimova; $i++) {
                                        if ($i < $brojTimova - 1) {
                                            echo "'".$teamData[$i]["Naziv"] . "',";
                                        } else {
                                            echo "'".$teamData[$i]["Naziv"] . "',";
                                        }
                                    }
                                ?>],
			datasets : [
				{
					fillColor : "rgba(220,220,220,0.5)",
					strokeColor : "rgba(220,220,220,1)",
					data : [<?php echo $podaci; ?>]
				}

			]
			
		}

	var myLine = new Chart(document.getElementById("canvas").getContext("2d")).Bar(barChartData);
	
	</script>
</body>
</html>
